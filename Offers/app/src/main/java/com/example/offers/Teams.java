package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.design.widget.TabLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class Teams extends AppCompatActivity implements MyTeam.OnFragmentInteractionListener, Available_Teams.OnFragmentInteractionListener, GoogleApiClient.OnConnectionFailedListener {

    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_teams,text_home,text_offers;
    String link="#--------------#";
    String code="1f2hh";
    private ActionBarDrawerToggle abdt;
    private DrawerLayout dl;
    TextView mName;
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    View header;
    NavigationView nav_view;
    public static int id;
    Intent intent;
    CircleImageView mPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);

        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_teams=findViewById(R.id.mteams);

        text_teams.setTextColor(Color.rgb(255,0,0));

        intent=getIntent();
        id=intent.getIntExtra("userid",0);

        getSupportActionBar().setTitle("Teams");

        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(Teams.this, MainActivity.class);
                intent1.putExtra("userid",id);
                startActivity(intent1);
                customType(Teams.this,"left-to-right");
                finish();


            }
        });

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(Teams.this, Launcher_Activity.class);
                intent1.putExtra("userid",id);
                startActivity(intent1);
                customType(Teams.this,"left-to-right");
                finish();

            }
        });

        getSupportActionBar().setTitle("Teams");

        TabLayout tabLayout=findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("MyTeam"));
        tabLayout.addTab(tabLayout.newTab().setText("Available_Teams"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager=findViewById(R.id.pager);
        final PagerAdapter1 adapter=new PagerAdapter1(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        dl.addDrawerListener(abdt);
        abdt.syncState();

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //gso.getAccount().;
        GoogleSignInAccount account;

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nav_view = (NavigationView) findViewById(R.id.nav_view);

       // header=nav_view.inflateHeaderView(R.layout.navigation_header);
       // mName=header.findViewById(R.id.nName);

        showProfile();

        //  prof.setImageResource(R.drawable.akshay);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int mid = menuItem.getItemId();
                // Uri u= Uri.parse("https://firebasestorage.googleapis.com/v0/b/farmcart-3f849.appspot.com/o/default%2Findex.png?alt=media&token=0088370f-41f4-402b-a26c-3c476135bc6a");
                // prof.setImageResource(R.drawable.akshay);


                if(mid==R.id.mProfile) {

                    Intent intent=new Intent(Teams.this,PersonalInfo.class);
                    intent.putExtra("tname","ABC");
                    intent.putExtra("flag","home");
                    intent.putExtra("userid",Teams.id);
                    startActivity(intent);


                } else if(mid==R.id.share) {

                    Intent shareintent=new Intent(Intent.ACTION_SEND);
                    shareintent.setType("text/plain");
                    String sharebody="Download the app by using this link "+link +" and use the refferal code "+code;
                    String sharesubject="STAY AWAY//subject";
                    shareintent.putExtra(Intent.EXTRA_TEXT,sharebody);
                    shareintent.putExtra(Intent.EXTRA_SUBJECT,sharesubject);
                    startActivity(Intent.createChooser(shareintent,"share using"));


                } else if(mid==R.id.setting) {



                } else if(mid==R.id.logout) {

                    /*startActivity(new Intent(Launcher_Activity.this,Login.class));
                    finish();*/

                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    if (status.isSuccess()){

                                        startActivity(new Intent(Teams.this,Login.class));
                                        finish();

                                    }else{
                                        Toast.makeText(getApplicationContext(),"Session not close",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }

                return true;


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.add_team)
        {
            Intent intent=new Intent(Teams.this,AddTeam.class);
            intent.putExtra("userid",Teams.id);
            startActivity(intent);
        }

        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }




    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent1=new Intent(Teams.this, Launcher_Activity.class);
        intent1.putExtra("userid",id);
        startActivity(intent1);
        customType(Teams.this,"left-to-right");
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

   /* @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account=result.getSignInAccount();
          //  Toast.makeText(this,account.getEmail(),Toast.LENGTH_SHORT).show();

            mName.setText(account.getDisplayName());


        }else{

        }
    }*/


    private void showProfile() {
        ConnectionClass connectionClass=new ConnectionClass();
        Connection conn=connectionClass.CONN();
        try {
            Statement stmt=conn.createStatement();
            String q="select * from User where userid="+id;
            ResultSet rs=stmt.executeQuery(q);
            if(rs.next()) {
                header=nav_view.inflateHeaderView(R.layout.navigation_header);
                mName=header.findViewById(R.id.nName);
                mName.setText(rs.getString("fname")+" "+rs.getString("lname"));
                mPhoto=header.findViewById(R.id.profm);
               // Picasso.with(Teams.this).load(rs.getString("profile")).into(mPhoto);
            }


        } catch (SQLException e) {
            Toast.makeText(Teams.this,e.getMessage().toString(),Toast.LENGTH_LONG).show();
        }


    }
}
