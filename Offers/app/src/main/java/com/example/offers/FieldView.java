package com.example.offers;

public class FieldView {

    String mImageName;
    int mpoints;
    int userid;
    public FieldView()
    {

    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public FieldView(String mImageName, int mpoints,int userid)
    {
        this.mImageName=mImageName;
        this.mpoints=mpoints;
        this.userid=userid;
    }

    public String getmImageName() {
        return mImageName;
    }

    public void setmImageName(String mImageName) {
        this.mImageName = mImageName;
    }

    public int getMpoints() {
        return mpoints;
    }

    public void setMpoints(int mpoints) {
        this.mpoints = mpoints;
    }
}
