package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class OfferInfo extends AppCompatActivity {

    private TextView text_offer;
    Intent intent;
    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_offers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_info);

        text_offer=findViewById(R.id.offer_info);
        intent=getIntent();
        text_offer.setText("Information about "+intent.getStringExtra("offer"));

        getSupportActionBar().setTitle(intent.getStringExtra("offer"));

        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_offers=findViewById(R.id.moffers);
        text_offers.setTextColor(Color.rgb(255,0,0));

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OfferInfo.this,Launcher_Activity.class));

                customType(OfferInfo.this,"right-to-left");
                finish();
            }
        });

        btn_teams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OfferInfo.this,Teams.class));
                customType(OfferInfo.this,"right-to-left");
                finish();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favourite,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.adddtf) {

            MainActivity.cmlist.add(intent.getStringExtra("offer") +" Comp. "+intent.getStringExtra("Cname"));
        }


        return true;
    }


}
