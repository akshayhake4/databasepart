package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class TeamProfile extends AppCompatActivity {

    TextView tvteamname;
    Intent intent;
    String teamName;
    private ArrayList<FieldView> mobj=new ArrayList<>();
    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_teams;
    int id=0;
    int tid=0;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_profile);

        intent=getIntent();
      teamName= intent.getStringExtra("tname");
      id=intent.getIntExtra("userid",0);
      tid=intent.getIntExtra("teamid",0);

        getSupportActionBar().setTitle(teamName);

        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_teams=findViewById(R.id.mteams);

        text_teams.setTextColor(Color.rgb(255,0,0));

        getSupportActionBar().setTitle("Teams");

        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TeamProfile.this,MainActivity.class));
                customType(TeamProfile.this,"left-to-right");
                finish();


            }
        });

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TeamProfile.this,Launcher_Activity.class));
                customType(TeamProfile.this,"left-to-right");
                finish();

            }
        });

        initImageBitmaps();

    }
    private void initImageBitmaps()
    {
        /*FieldView obj=new FieldView("sanket",20);
        mobj.add(obj);
        obj=new FieldView("akshay",15);
        mobj.add(obj);
        obj=new FieldView("hake",15);
        mobj.add(obj);
        obj=new FieldView("gattani",10);
        mobj.add(obj);
        obj=new FieldView("aditya",10);
        mobj.add(obj);
        obj=new FieldView("kadam",10);
        mobj.add(obj);
        obj=new FieldView("saanchi",10);
        mobj.add(obj);
        obj=new FieldView("chaitanya",10);
        mobj.add(obj);*/

        ConnectionClass connectionClass=new ConnectionClass();
        Connection conn=connectionClass.CONN();

        try {
            Statement statement=conn.createStatement();
            String q="select fname,earnedpoints from User INNER JOIN points on points.userid=User.userid INNER JOIN UserTeam on UserTeam.userid=points.userid WHERE UserTeam.teamid="+tid;
            ResultSet rs=statement.executeQuery(q);

            while (rs.next())
            {
                FieldView obj=new FieldView(rs.getString("fname"),rs.getInt("earnedpoints"),0);
                mobj.add(obj);
            }
        } catch (SQLException e) {
            Toast.makeText(TeamProfile.this,e.getMessage(),Toast.LENGTH_SHORT).show();
        }


        initRecyclerView();
    }

    private  void initRecyclerView()
    {
        RecyclerView recyclerView=findViewById(R.id.recycler_view);
        PersonalRecyclerViewAdapter adapter=new PersonalRecyclerViewAdapter(this,mobj);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.join,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int mid=item.getItemId();

        if(mid==R.id.join_team) {

            ConnectionClass connectionClass=new ConnectionClass();
            Connection con=connectionClass.CONN();
            Calendar calendar=Calendar.getInstance();

            try {
                Statement stmt=con.createStatement();
                String time=calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH)+" "+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);
                String q="insert into UserTeam values("+id+","+tid+",'"+time+"','"+time+"')";
                stmt.executeUpdate(q);

                Intent intent=new Intent(TeamProfile.this,Teams.class);
                intent.putExtra("userid",id);
                startActivity(intent);
                finish();


            } catch (SQLException e) {
                Toast.makeText(TeamProfile.this,e.getMessage()+" "+id+"  "+tid,Toast.LENGTH_SHORT).show();
            }


        }



        return true;
    }

}
