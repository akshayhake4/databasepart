package com.example.offers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

public class Login extends AppCompatActivity {

    private EditText email,password;
    private Button login,signup;
    private RelativeLayout Luname,Lpwd;
    GoogleSignInClient mGoogleSignInClient;
    private static int RC_SIGN_IN = 1;
    SignInButton btngoogle;
    ProgressDialog progressDialog;
    LoginButton btnfb;
    CallbackManager callbackManager;
    ConnectionClass connectionClass ;
    int id=0;
    int id1=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        connectionClass=new ConnectionClass();



        email=findViewById(R.id.textEmail);
        password=findViewById(R.id.textPass);
        login=findViewById(R.id.btnlogin);
        signup=findViewById(R.id.btnsignup);
        Luname=findViewById(R.id.Luname);
        Lpwd=findViewById(R.id.Lpass);
        btngoogle=findViewById(R.id.btngoogle);
        progressDialog=new ProgressDialog(this);
        btnfb=findViewById(R.id.btnfb);

        callbackManager=CallbackManager.Factory.create();

        printHashKey(this);

      //  LoginManager.getInstance().logOut();
        btnfb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

              String s=  loginResult.getAccessToken().getUserId();

              //  Toast.makeText(Login.this,loginResult.getClass().getName(),Toast.LENGTH_LONG).show();

            //  Toast.makeText(Login.this,"Successfull ",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(Login.this,Launcher_Activity.class);
                intent.putExtra("flag","facebook");
                startActivity(intent);
                finish();



            }

            @Override
            public void onCancel() {
                Toast.makeText(Login.this,"Cancel ",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(Login.this,"Error ",Toast.LENGTH_LONG).show();
            }
        });




        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);



        btngoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        signIn();


            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=email.getText().toString();
                String pwd=password.getText().toString();

                if(TextUtils.isEmpty(username) && TextUtils.isEmpty(pwd)) {

                    Luname.setBackgroundResource(R.drawable.redborder);
                    Lpwd.setBackgroundResource(R.drawable.redborder);
                    Toast.makeText(Login.this,"Empty Fields",Toast.LENGTH_SHORT).show();

                }else if(TextUtils.isEmpty(username)) {

                    Luname.setBackgroundResource(R.drawable.redborder);
                    Lpwd.setBackgroundResource(R.drawable.border);
                    Toast.makeText(Login.this,"Enter Email first",Toast.LENGTH_SHORT).show();

                }
                else if(TextUtils.isEmpty(pwd)) {
                    Lpwd.setBackgroundResource(R.drawable.redborder);
                    Luname.setBackgroundResource(R.drawable.border);
                    Toast.makeText(Login.this,"Enter Password first",Toast.LENGTH_SHORT).show();
                } else {


                    Connection conn=connectionClass.CONN();
                    String q="select * from User where email='"+username+"'and password='"+pwd+"'";
                    Statement st= null;
                    try {
                        st = conn.createStatement();
                        ResultSet rs=st.executeQuery(q);
                        if(rs.next()) {
                            Toast.makeText(Login.this,"LOGIN successful",Toast.LENGTH_SHORT).show();
                            id=rs.getInt("userid");
                            Intent intent=new Intent(Login.this,Launcher_Activity.class);
                            intent.putExtra("flag","login");
                            intent.putExtra("userid",id);
                            startActivity(intent);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }





                }

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,Signup.class));
            }
        });

    }

    private void signIn() {
        progressDialog.setMessage("Please wait");

        progressDialog.show();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent,RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

       // Toast.makeText(Login.this,"hello ",Toast.LENGTH_LONG).show();
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);


        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            int id=0;
            String res = "";
            try {
                Connection con = connectionClass.CONN();
                if (con == null) {
                    res="error";
                } else {

                    String q="select userid from User order by userid desc limit 1";
                    Statement stmt1 = con.createStatement();
                    ResultSet rs=stmt1.executeQuery(q);

                    if(rs.next())
                        id=rs.getInt("userid");

                    q="select teamid from Team order by teamid desc limit 1";
                    stmt1 = con.createStatement();
                    rs=stmt1.executeQuery(q);

                    if(rs.next())
                        id1=rs.getInt("teamid");

                    if(id1>id)
                        id1=id;
                    id++;


                    String f_name=account.getGivenName();
                    String l_name=account.getFamilyName();
                    String phone="";
                    String mpwd1="";
                    String email=account.getEmail();
                    String prof="";
                    if(account.getPhotoUrl()==null)
                    {
                        prof="";
                    }
                    else
                        prof=account.getPhotoUrl().toString();
                    Calendar calendar=Calendar.getInstance();
                    String time=calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH)+" "+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);
                    String ref="xyz";
                    q="select * from User where email='"+email+"'";
                    rs=stmt1.executeQuery(q);
                    if(rs.next()) {
                        id=rs.getInt("userid");
                        Intent intent=new Intent(Login.this,Launcher_Activity.class);
                        intent.putExtra("flag","google");
                        intent.putExtra("userid",id);
                        startActivity(intent);

                    }
                    String query="insert into User values("+id+",'"+f_name+"','"+l_name+"','"+email+"','"+phone+"','"+f_name+"','"+mpwd1+"','"+prof+"','"+ref+"','"+time+"','"+time+"','"+time+"')";

                    Statement stmt = con.createStatement();
                    stmt.executeUpdate(query);

                    q="insert into points(userid) values("+id+")";
                    stmt.executeUpdate(q);

                    res="inserted";

                    Intent intent=new Intent(Login.this,Launcher_Activity.class);
                    intent.putExtra("flag","google");
                    intent.putExtra("userid",id);
                    startActivity(intent);


                }
            }
            catch (Exception ex)
            {
                Toast.makeText(Login.this, ex.getMessage(),Toast.LENGTH_LONG).show();

            }

            //Toast.makeText(Login.this,account.getEmail(),Toast.LENGTH_LONG).show();
            String name=account.getDisplayName();
            progressDialog.cancel();



        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.


        }
    }

    public static void printHashKey(Context context) {
        try {
            final PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                final String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("AppLog", "key:" + hashKey + "=");
               // Toast.makeText(context,"key:" + hashKey + "=",Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            Log.e("AppLog", "error:", e);
        }

    }



}
