package com.example.offers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonalRecyclerViewAdapter extends RecyclerView.Adapter<PersonalRecyclerViewAdapter.ViewHolder> {

    private ArrayList<FieldView> mobj= new ArrayList<>();

    private Context mcontext;

    public PersonalRecyclerViewAdapter(Context mcontext, ArrayList<FieldView> mobj) {
        this.mobj=mobj;
        this.mcontext = mcontext;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        final FieldView obj=mobj.get(position);
        viewHolder.imageName.setText(obj.getmImageName());
       // viewHolder.imageName.setText("1");
        viewHolder.points.setText(String.valueOf(obj.getMpoints()));

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mcontext,obj.getmImageName(),Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(mcontext, PersonalInfo.class);
                intent.putExtra("tname",obj.getmImageName());
                intent.putExtra("flag","teams");
                intent.putExtra("userid",Teams.id);
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mobj.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        TextView imageName;
        TextView points;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            imageName=itemView.findViewById(R.id.image_name);
            points=itemView.findViewById(R.id.points);
            parentLayout=itemView.findViewById(R.id.parent_layout);
        }

    }
}

