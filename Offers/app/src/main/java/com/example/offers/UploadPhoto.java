package com.example.offers;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.hdodenhof.circleimageview.CircleImageView;

public class UploadPhoto extends AppCompatActivity {

    private Button skip,upload,done;
    private static final int GALLERY_INTENT = 2;
    private CircleImageView profile;
    private Intent intent;
    private TextView uuname,umob,mName;
    int id;
    ConnectionClass  connectionClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_photo);

        connectionClass=new ConnectionClass();

        intent=getIntent();
        id=intent.getIntExtra("userid",1);
        upload=findViewById(R.id.btnupload1);
        done=findViewById(R.id.btndone1);
        profile=findViewById(R.id.profilePhoto);
        uuname=findViewById(R.id.textuEmail);
        umob=findViewById(R.id.sumob);
        mName=findViewById(R.id.mName);

        Connection connection=connectionClass.CONN();
        if(connection==null) {

        }
        else
        {
            try {
                Statement statement=connection.createStatement();
                String q="select * from User where userid="+id;
                ResultSet rs=statement.executeQuery(q);
                if(rs.next()) {
                      mName.setText(rs.getString("fname")+" "+rs.getString("lname"));
                      uuname.setText(rs.getString("email"));
                      umob.setText(rs.getString("mobile"));
                }

                 q="insert into points(userid) values("+id+")";
                statement.executeUpdate(q);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

       /* mName.setText(intent.getStringExtra("name"));
        uuname.setText(intent.getStringExtra("uname"));
        umob.setText(intent.getStringExtra("mob"));*/


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(UploadPhoto.this,Launcher_Activity.class);
                intent.putExtra("userid",id);
                startActivity(intent);

                finish();
            }
        });




        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,GALLERY_INTENT);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode== GALLERY_INTENT && resultCode== RESULT_OK)
        {




            Uri uri=data.getData();
            Picasso.with(UploadPhoto.this).load(uri.toString()).into(profile);

            Connection conn=connectionClass.CONN();
            String q="update User set profile='"+uri.toString()+"' where userid="+id;
            try {
                Statement stmt=conn.createStatement();
                stmt.executeUpdate(q);
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }

    }
}
