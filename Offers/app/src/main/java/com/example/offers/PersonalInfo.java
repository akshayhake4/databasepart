package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class PersonalInfo extends AppCompatActivity {

    TextView membername,memail,mmob;
    Intent intent;
    String name;
    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_teams;
    String flag;
    int uid=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);
        membername=findViewById(R.id.membername);
        memail=findViewById(R.id.tvemail);
        mmob=findViewById(R.id.tvmob);
        intent=getIntent();
        name= intent.getStringExtra("tname");
        uid=intent.getIntExtra("userid",0);


        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_teams=findViewById(R.id.mteams);


        text_teams.setTextColor(Color.rgb(255,0,0));

        showProfile();

        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PersonalInfo.this,MainActivity.class);
                intent.putExtra("userid",uid);
                startActivity(intent);
                customType(PersonalInfo.this,"left-to-right");
                finish();


            }
        });

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PersonalInfo.this,Launcher_Activity.class);
                intent.putExtra("userid",uid);
                startActivity(intent);
                customType(PersonalInfo.this,"left-to-right");
                finish();

            }
        });

        getSupportActionBar().setTitle("Profile");
    }

    private void showProfile() {
        ConnectionClass connectionClass=new ConnectionClass();
        Connection conn=connectionClass.CONN();

        try {
            Statement statement=conn.createStatement();
            String q="select fname,lname,email,mobile from User where userid="+uid;
            ResultSet rs=statement.executeQuery(q);
            if(rs.next())
            {
                membername.setText(rs.getString("fname")+" "+rs.getString("lname"));
                memail.setText(rs.getString("email"));
                mmob.setText(rs.getString("mobile"));
            }
        } catch (SQLException e) {
            Toast.makeText(PersonalInfo.this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

        flag=intent.getStringExtra("flag");
        if(flag.equals("home")) {

            Intent intent=new Intent(PersonalInfo.this,Launcher_Activity.class);
            intent.putExtra("userid",uid);
            startActivity(intent);

            customType(PersonalInfo.this,"right-to-left");
        }

        super.onBackPressed();
    }
}
