package com.example.offers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.ViewHolder> {

    private Context mContext;
    private List<OfferView> mOffers;

    public OfferAdapter(Context mContext, List<OfferView> mOffrs) {
        this.mContext = mContext;
        this.mOffers = mOffrs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.offer_item,viewGroup,false);
        return new OfferAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final OfferView offer=mOffers.get(i);

        viewHolder.name.setText(offer.getView_name());
        viewHolder.desc.setText(offer.getView_desc());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,OfferInfo.class);
                intent.putExtra("offer",offer.getView_name());
                intent.putExtra("Cname",offer.getC_name());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mOffers.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView offerLogo;
        public TextView name,desc;

        public ViewHolder(View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.offerName);
            desc=itemView.findViewById(R.id.offerDes);
            offerLogo=itemView.findViewById(R.id.offerLogo);



        }
    }
}
