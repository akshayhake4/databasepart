package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private RecyclerView recyclerView;
    private List<TypeView> mTypes;
    private TypeAdapter typeAdapter;
    private SearchView searchView;
    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_offers;
    String link="#--------------#";
    String code="1f2hh";
    private ActionBarDrawerToggle abdt;
    private DrawerLayout dl;
    TextView mName;
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    View header;
    NavigationView nav_view;
    int uid=0;
    CircleImageView mPhoto;

    public static ArrayList<String> cmlist=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // startActivity(new Intent(this,Company.class));
        getSupportActionBar().setTitle("Offers");

        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_offers=findViewById(R.id.moffers);
        text_offers.setTextColor(Color.rgb(255,0,0));

        uid=getIntent().getIntExtra("userid",0);

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Launcher_Activity.class);
                intent.putExtra("userid",uid);
                startActivity(intent);
                customType(MainActivity.this,"right-to-left");
                finish();
            }
        });

        btn_teams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Teams.class);
                intent.putExtra("userid",uid);
                startActivity(intent);
                customType(MainActivity.this,"right-to-left");
                finish();

            }
        });



        recyclerView=findViewById(R.id.recycler_view_type);
        searchView=(SearchView)findViewById(R.id.searchView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mTypes=new ArrayList<>();

        readTypes();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                typeAdapter.getFilter().filter(newText);

                return false;
            }
        });


        dl.addDrawerListener(abdt);
        abdt.syncState();

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //gso.getAccount().;
        GoogleSignInAccount account;

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nav_view = (NavigationView) findViewById(R.id.nav_view);

        header=nav_view.inflateHeaderView(R.layout.navigation_header);
        mName=header.findViewById(R.id.nName);
        //  prof.setImageResource(R.drawable.akshay);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                // Uri u= Uri.parse("https://firebasestorage.googleapis.com/v0/b/farmcart-3f849.appspot.com/o/default%2Findex.png?alt=media&token=0088370f-41f4-402b-a26c-3c476135bc6a");
                // prof.setImageResource(R.drawable.akshay);


                if(id==R.id.mProfile) {

                    Intent intent=new Intent(MainActivity.this,PersonalInfo.class);
                    intent.putExtra("tname","ABC");
                    intent.putExtra("flag","home");
                    startActivity(intent);


                } else if(id==R.id.share) {

                    Intent shareintent=new Intent(Intent.ACTION_SEND);
                    shareintent.setType("text/plain");
                    String sharebody="Download the app by using this link "+link +" and use the refferal code "+code;
                    String sharesubject="STAY AWAY//subject";
                    shareintent.putExtra(Intent.EXTRA_TEXT,sharebody);
                    shareintent.putExtra(Intent.EXTRA_SUBJECT,sharesubject);
                    startActivity(Intent.createChooser(shareintent,"share using"));


                } else if(id==R.id.setting) {



                } else if(id==R.id.logout) {

                    /*startActivity(new Intent(Launcher_Activity.this,Login.class));
                    finish();*/

                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    if (status.isSuccess()){

                                        startActivity(new Intent(MainActivity.this,Login.class));
                                        finish();

                                    }else{
                                        Toast.makeText(getApplicationContext(),"Session not close",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }

                return true;


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    public void readTypes() {
        String [] list={"Fashion","Recharge","Food Orders","Books and Stationary","Home and Furniture","TVs and Appliances","Mobiles and Tablets","Electronics","Travel","Cosmetics"};
        String [] companyList={"1","2","3","4","5","5","6","7","8","9","10"};

        if(cmlist.size()!=0) {
            TypeView favourite = new TypeView("MY FAVOURITES", GetStringArray(cmlist));
            mTypes.add(favourite);
        }

        for(int i=1;i<10;i++) {

            TypeView typeView=new TypeView(list[i],companyList);
            mTypes.add(typeView);
        }

        typeAdapter=new TypeAdapter(MainActivity.this,mTypes);
        recyclerView.setAdapter(typeAdapter);

    }

    public static String[] GetStringArray(ArrayList<String> arr)
    {

        // declaration and initialise String Array
        String str[] = new String[arr.size()];

        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {

            // Assign each value to String array
            str[j] = arr.get(j);
        }

        return str;
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(MainActivity.this,Launcher_Activity.class);
        intent.putExtra("userid",uid);
        startActivity(intent);
        customType(MainActivity.this,"right-to-left");
        finish();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account=result.getSignInAccount();
            Toast.makeText(this,account.getEmail(),Toast.LENGTH_SHORT).show();

            mName.setText(account.getDisplayName());


        }else{

        }
    }

    private void showProfile() {
        ConnectionClass connectionClass=new ConnectionClass();
        Connection conn=connectionClass.CONN();
        try {
            Statement stmt=conn.createStatement();
            String q="select * from User where userid="+uid;
            ResultSet rs=stmt.executeQuery(q);
            if(rs.next()) {
                header=nav_view.inflateHeaderView(R.layout.navigation_header);
                mName=header.findViewById(R.id.nName);
                mName.setText(rs.getString("fname")+" "+rs.getString("lname"));
                mPhoto=header.findViewById(R.id.profm);
               // Picasso.with(MainActivity.this).load(rs.getString("profile")).into(mPhoto);
            }


        } catch (SQLException e) {
            Toast.makeText(MainActivity.this,e.getMessage().toString(),Toast.LENGTH_LONG).show();
        }


    }
}
