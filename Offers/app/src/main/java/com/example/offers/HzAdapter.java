package com.example.offers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class HzAdapter extends RecyclerView.Adapter<HzAdapter.ViewHolder> {

    private Context mContext;
    private List<CompanyView> mCompanies;

    public HzAdapter(Context mContext, List<CompanyView> mCompanies) {
        this.mContext = mContext;
        this.mCompanies = mCompanies;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.company_item,viewGroup,false);
        return new HzAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final CompanyView companyView=mCompanies.get(i);

        viewHolder.hz_name.setText(companyView.getCompany_name());

        if(companyView.getType().equals("MY FAVOURITES")) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext,OfferInfo.class);
                    intent.putExtra("offer",companyView.getCompany_name());
                    intent.putExtra("Cname", companyView.getCompany_name());
                    mContext.startActivity(intent);
                }
            });
        } else {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, Company.class);
                    intent.putExtra("Cname", companyView.getCompany_name());
                    mContext.startActivity(intent);

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mCompanies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView hz_company_logo;
        private TextView hz_name;

        public ViewHolder(View itemView) {
            super(itemView);

            hz_company_logo=itemView.findViewById(R.id.hz_logo);
            hz_name=itemView.findViewById(R.id.hz_name);



        }
    }
}
