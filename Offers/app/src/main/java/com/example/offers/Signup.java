package com.example.offers;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Calendar;

public class Signup extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private EditText fname,semail,pwd1,pwd2,lname,mob;
    private TextView bdate;
    private Button next;
    private RelativeLayout R_email,R_phone,R_pwd1,R_pwd2;
    ConnectionClass connectionClass;
    int id1=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        fname=findViewById(R.id.fname);
        bdate=findViewById(R.id.bdate);
        semail=findViewById(R.id.semail);
        pwd1=findViewById(R.id.pwd1);
        pwd2=findViewById(R.id.pwd2);
        next=findViewById(R.id.btnnext);
        lname=findViewById(R.id.lname);
        mob=findViewById(R.id.smob);
        R_email=findViewById(R.id.Lsuname);
        R_phone=findViewById(R.id.Lsmob);
        R_pwd1=findViewById(R.id.Lspass);
        R_pwd2=findViewById(R.id.Lspass2);
        connectionClass = new ConnectionClass();


        bdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datepicker=new DatePickerFragment();
                datepicker.show(getSupportFragmentManager(),"date picker");
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String f_name,l_name,b_date,email,phone,mpwd1,mpwd2;
                f_name=fname.getText().toString();
                l_name=lname.getText().toString();
                b_date=bdate.getText().toString();
                email=semail.getText().toString();
                phone=mob.getText().toString();
                mpwd1=pwd1.getText().toString();
                mpwd2=pwd2.getText().toString();
                boolean flag=true;


                int id=0;
                if(TextUtils.isEmpty(f_name) ) {
                    fname.setBackgroundResource(R.drawable.redborder);
                    flag=false;

                }  else {
                    fname.setBackgroundResource(R.drawable.border);
                }

                if(TextUtils.isEmpty(l_name)) {
                    lname.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    lname.setBackgroundResource(R.drawable.border);

                }

                if(TextUtils.isEmpty(b_date)) {
                    bdate.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    bdate.setBackgroundResource(R.drawable.border);
                }

                if(TextUtils.isEmpty(email)) {
                    R_email.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    R_email.setBackgroundResource(R.drawable.border);
                }


                if(TextUtils.isEmpty(phone)) {
                    R_phone.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    R_phone.setBackgroundResource(R.drawable.border);
                }

                if(TextUtils.isEmpty(mpwd1)) {
                    R_pwd1.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    R_pwd1.setBackgroundResource(R.drawable.border);
                }


                if(TextUtils.isEmpty(mpwd2)) {
                    R_pwd2.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                } else {
                    R_pwd2.setBackgroundResource(R.drawable.border);
                }

                if(!mpwd1.equals(mpwd2)) {
                    Toast.makeText(Signup.this,"Password not matching",Toast.LENGTH_SHORT).show();
                    R_pwd2.setBackgroundResource(R.drawable.redborder);
                    flag=false;
                }

                if(flag) {

                    String res = "";
                    try {
                        Connection con = connectionClass.CONN();
                        if (con == null) {
                            res="error";
                        } else {

                            String q="select userid from User order by userid desc limit 1";
                            Statement stmt1 = con.createStatement();
                            ResultSet rs=stmt1.executeQuery(q);

                            if(rs.next())
                                id=rs.getInt("userid");

                            q="select teamid from Team order by teamid desc limit 1";
                            stmt1 = con.createStatement();
                            rs=stmt1.executeQuery(q);

                            if(rs.next())
                                id1=rs.getInt("teamid");

                            if(id1>id)
                                id1=id;
                            id++;
                            String prof="1";
                            String ref="xyz";
                            Calendar calendar=Calendar.getInstance();
                            String time=calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH)+" "+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);

                            res="before";
                            String query="insert into User values("+id+",'"+f_name+"','"+l_name+"','"+email+"','"+phone+"','"+f_name+"','"+mpwd1+"','"+prof+"','"+ref+"','"+time+"','"+time+"','"+time+"')";

                            Statement stmt = con.createStatement();
                            stmt.executeUpdate(query);

                            res="inserted";


                        }
                    }
                    catch (Exception ex)
                    {
                        res=ex.getMessage().toString();
                    }

                    Toast.makeText(Signup.this,res,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Signup.this, UploadPhoto.class);
                    intent.putExtra("name", fname.getText().toString() + " " + lname.getText().toString());
                    intent.putExtra("uname", semail.getText().toString());
                    intent.putExtra("mob", mob.getText().toString());
                    intent.putExtra("flag","signup");
                    intent.putExtra("userid",id);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar c=Calendar.getInstance();
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);

        String curdate= DateFormat.getDateInstance().format(c.getTime());
        bdate.setText(curdate);

    }
}
