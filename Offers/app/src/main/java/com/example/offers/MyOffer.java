package com.example.offers;

public class MyOffer {

    private String logoUrl,offer_name,offer_desc;

    public MyOffer(String logoUrl, String offer_name, String offer_desc) {
        this.logoUrl = logoUrl;
        this.offer_name = offer_name;
        this.offer_desc = offer_desc;
    }

    public MyOffer() {

    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }

    public String getOffer_desc() {
        return offer_desc;
    }

    public void setOffer_desc(String offer_desc) {
        this.offer_desc = offer_desc;
    }
}
