package com.example.offers;

public class TypeView {

    private String typeName;
    private String[] imageList;

    public TypeView() {

    }

    public TypeView(String typeName, String[] imageList) {
        this.typeName = typeName;
        this.imageList = imageList;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String[] getImageList() {
        return imageList;
    }

    public void setImageList(String[] imageList) {
        this.imageList = imageList;
    }
}
