package com.example.offers;

public class CompanyView {

    private String company_logo_url,company_name,type;

    public CompanyView() {

    }

    public CompanyView(String company_logo_url, String company_name,String type) {
        this.company_logo_url = company_logo_url;
        this.company_name = company_name;
        this.type=type;

    }

    public String getCompany_logo_url() {
        return company_logo_url;
    }

    public void setCompany_logo_url(String company_logo_url) {
        this.company_logo_url = company_logo_url;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
