package com.example.offers;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.carrier.CarrierMessagingService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class Launcher_Activity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener    {

    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_home;
    private TextView txtMinute, txtSecond,txtPoints;
    private static final String CHANNEL_ID = "channel1";
    private Handler handler;
    private Runnable runnable;
    int minutes,seconds;
    long points=0;
    boolean f=false;
    private Toolbar toolbar;
    String link="#--------------#";
    String code="1f2hh";
    private ActionBarDrawerToggle abdt;
    private DrawerLayout dl;
    TextView mName;
    CircleImageView mPhoto;
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    View header;
    NavigationView nav_view;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    ProfileTracker profileTracker;
    String flag;
    int id;
    ConnectionClass connectionClass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher_);


        getSupportActionBar().setTitle("Home");

        final Intent intent=getIntent();
        flag=intent.getStringExtra("flag");
        id=intent.getIntExtra("userid",0);

        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        btn_teams = findViewById(R.id.teams);
        btn_home = findViewById(R.id.home);
        btn_offers = findViewById(R.id.offers);
        text_home = findViewById(R.id.mhome);
        nav_view = (NavigationView) findViewById(R.id.nav_view);
        txtPoints = findViewById(R.id.points);
        text_home.setTextColor(Color.rgb(255, 0, 0));
        connectionClass=new ConnectionClass();


       // mName.setText(getIntent().getStringExtra("name"));

        showProfile();


        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(Launcher_Activity.this, MainActivity.class);
                intent1.putExtra("userid",id);
                startActivity(intent1);
                customType(Launcher_Activity.this, "left-to-right");

            }
        });

        btn_teams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(Launcher_Activity.this, Teams.class);
                intent1.putExtra("userid",id);
                startActivity(intent1);
                customType(Launcher_Activity.this, "right-to-left");

            }

        });

        txtMinute = findViewById(R.id.textMin);
        txtSecond = findViewById(R.id.textSec);


       // SharedPreferences sp = getSharedPreferences("com.example.offers.mainpage.Launcher_Activity", Context.MODE_PRIVATE);
       // points = sp.getLong("points", 0);

       points=Integer.parseInt(txtPoints.getText().toString());

       // txtPoints.setText("" + String.format("%02d", points));

        countDownStart();

        dl.addDrawerListener(abdt);
        abdt.syncState();

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //gso.getAccount().;
        GoogleSignInAccount account;

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        //  prof.setImageResource(R.drawable.akshay);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int mid = menuItem.getItemId();
               // Uri u= Uri.parse("https://firebasestorage.googleapis.com/v0/b/farmcart-3f849.appspot.com/o/default%2Findex.png?alt=media&token=0088370f-41f4-402b-a26c-3c476135bc6a");
                // prof.setImageResource(R.drawable.akshay);


                if(mid==R.id.mProfile) {

                    Intent intent=new Intent(Launcher_Activity.this,PersonalInfo.class);
                    intent.putExtra("tname","ABC");
                    intent.putExtra("flag","home");
                    intent.putExtra("userid",id);
                    startActivity(intent);


                } else if(mid==R.id.share) {

                    Intent shareintent=new Intent(Intent.ACTION_SEND);
                    shareintent.setType("text/plain");
                    String sharebody="Download the app by using this link "+link +" and use the refferal code "+code;
                    String sharesubject="STAY AWAY//subject";
                    shareintent.putExtra(Intent.EXTRA_TEXT,sharebody);
                    shareintent.putExtra(Intent.EXTRA_SUBJECT,sharesubject);
                    startActivity(Intent.createChooser(shareintent,"share using"));


                } else if(mid==R.id.setting) {



                } else if(mid==R.id.logout) {

                    /*startActivity(new Intent(Launcher_Activity.this,Login.class));
                    finish();*/


                        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        if (status.isSuccess()) {

                                            startActivity(new Intent(Launcher_Activity.this, Login.class));
                                            finish();
                                            return ;

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Session not close", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });


                        LoginManager.getInstance().logOut();
                        startActivity(new Intent(Launcher_Activity.this, Login.class));
                        finish();


                }

                return true;


            }
        });


        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


       accessTokenTracker=new AccessTokenTracker() {
           @Override
           protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
              // loaduser(currentAccessToken);
           }
       };


       if(LoginManager.getInstance()!=null) {
           accessToken = AccessToken.getCurrentAccessToken();
           loaduser(accessToken);

       }




    }

    private void showProfile() {
        Connection conn=connectionClass.CONN();
        try {
            Statement stmt=conn.createStatement();
            String q="select * from User where userid="+id;
            ResultSet rs=stmt.executeQuery(q);
            if(rs.next()) {
                header=nav_view.inflateHeaderView(R.layout.navigation_header);
                mName=header.findViewById(R.id.nName);
                mName.setText(rs.getString("fname")+" "+rs.getString("lname"));
                mPhoto=header.findViewById(R.id.profm);
               // Picasso.with(Launcher_Activity.this).load(rs.getString("profile")).into(mPhoto);
            }

            q="select earnedpoints from points where userid="+id;
            rs=stmt.executeQuery(q);
            if(rs.next()) {
                txtPoints.setText(String.valueOf(rs.getInt("earnedpoints")));
            }
        } catch (SQLException e) {
            Toast.makeText(Launcher_Activity.this,e.getMessage().toString(),Toast.LENGTH_LONG).show();
        }


    }

    private void loaduser(AccessToken mToken) {

        GraphRequest request=GraphRequest.newMeRequest(mToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {
                    if(object==null) return;

                    Toast.makeText(Launcher_Activity.this,"getting profile",Toast.LENGTH_SHORT).show();

                    String fname=object.getString("first_name");
                    String lname=object.getString("last_name");
                    String id=object.getString("id");
                    String image_url="https://graph.facebook.com/"+id+"/picture?type=normal";

                    header=nav_view.inflateHeaderView(R.layout.navigation_header);
                    mName=header.findViewById(R.id.nName);
                    mName.setText(fname+" "+lname);
                    mPhoto=header.findViewById(R.id.profm);
                    Picasso.with(Launcher_Activity.this).load(image_url).into(mPhoto);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Bundle bundle=new Bundle();
        bundle.putString("fields","first_name,last_name,email,id");
        request.setParameters(bundle);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }



    public void countDownStart() {
        seconds=59;
        minutes=9;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {

                        seconds--;
                        if (seconds == -1) {
                            seconds = 59;
                            minutes--;
                        }
                        if (minutes == -1) {
                            points++;
                            Connection conn=connectionClass.CONN();

                            Calendar calendar=Calendar.getInstance();
                            String time=calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH)+" "+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);

                            String q="update points set lastmodifiedtime='"+time+"',earnedpoints="+points+" where userid="+id;
                            Statement stmt=conn.createStatement();
                            stmt.executeUpdate(q);
                            android.content.SharedPreferences sp = getSharedPreferences("com.example.offers.mainpage.Launcher_Activity", Context.MODE_PRIVATE);
                            sp.edit().putLong("points", points).commit();
                            minutes = 9;
                            seconds = 59;
                            KeyguardManager myKM = (KeyguardManager) getBaseContext().getSystemService(Context.KEYGUARD_SERVICE);
                            if (myKM.inKeyguardRestrictedInputMode()) { //app locked
                                notifyme();
                            }
                        }
                        txtMinute.setText("" + String.format("%02d", minutes));
                        txtSecond.setText("" + String.format("%02d", seconds));
                        txtPoints.setText("" + String.format("%02d", points));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1 * 1000);
    }
    public void notifyme(){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Notification");
            NotificationManager notificationManager=getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(Launcher_Activity.this, CHANNEL_ID)
                .setContentTitle("Congratulations")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentText("New point credited")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat;
        notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(1, builder.build());
    }


    @Override
    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

   /* @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account=result.getSignInAccount();
            Toast.makeText(this,account.getEmail(),Toast.LENGTH_SHORT).show();
            header=nav_view.inflateHeaderView(R.layout.navigation_header);
            mName=header.findViewById(R.id.nName);
            mName.setText(account.getDisplayName());
            mPhoto=header.findViewById(R.id.profm);
            Picasso.with(Launcher_Activity.this).load(account.getPhotoUrl()).into(mPhoto);


        }else{

        }
    }*/
}
