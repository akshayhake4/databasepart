package com.example.offers;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static maes.tech.intentanim.CustomIntent.customType;

public class Company extends AppCompatActivity {

    private RecyclerView recyclerView;
    private OfferAdapter offerAdapter;
    private List<OfferView> mOffers;
    Intent intent;
    private TextView companyName;
    private ScrollView scrollView;
    private CircleImageView btn_teams,btn_home,btn_offers;
    private TextView text_offers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        btn_teams=findViewById(R.id.teams);
        btn_home=findViewById(R.id.home);
        btn_offers=findViewById(R.id.offers);
        text_offers=findViewById(R.id.moffers);
        text_offers.setTextColor(Color.rgb(255,0,0));

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Company.this,Launcher_Activity.class));

                customType(Company.this,"right-to-left");
                finish();
            }
        });

        btn_teams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Company.this,Teams.class));
                customType(Company.this,"right-to-left");
                finish();

            }
        });

        intent=getIntent();
        getSupportActionBar().setTitle("Offes from Company " + intent.getStringExtra("Cname"));

        companyName=findViewById(R.id.companyName);
        companyName.setText("Company "+intent.getStringExtra("Cname"));
        recyclerView=findViewById(R.id.recycler_view_offer);
        recyclerView.setNestedScrollingEnabled(false);
       // scrollView=findViewById(R.id.scrollView);
       // scrollView.fullScroll(View.FOCUS_DOWN);
      //  scrollView.setSmoothScrollingEnabled(true);
       // recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mOffers=new ArrayList<>();

        readOffers();


    }

    public void readOffers() {

        for(int i=1;i<=10;i++) {
            OfferView offerView=new OfferView("xyz","Offer "+i,"Here is offer "+i,intent.getStringExtra("Cname"));
            mOffers.add(offerView);
        }

        offerAdapter=new OfferAdapter(Company.this,mOffers);
        recyclerView.setAdapter(offerAdapter);
    }
}
