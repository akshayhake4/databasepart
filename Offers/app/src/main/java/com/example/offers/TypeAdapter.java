package com.example.offers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.ViewHolder> implements Filterable {

    private Context mContext;
    private List<TypeView> mTypes;
    private List<TypeView> mTypesFull;
    private ArrayList<CompanyView> daysList=new ArrayList<>();

    public TypeAdapter(Context mContext, List<TypeView> mTypes) {
        this.mContext = mContext;
        this.mTypes = mTypes;
        mTypesFull=new ArrayList<>(mTypes);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view= LayoutInflater.from(mContext).inflate(R.layout.type_item,viewGroup,false);
       return new TypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        TypeView typeView=mTypes.get(i);

        viewHolder.typeName.setText(typeView.getTypeName());
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,false);
        viewHolder.type_recycler_view.setLayoutManager(layoutManager);
        viewHolder.type_recycler_view.setHasFixedSize(true);
        daysList.clear();
        String [] days=typeView.getImageList();
        if(typeView.getTypeName().equals("MY FAVOURITES")) {

        }
        for(int j=0;j<days.length;j++) {
            CompanyView companyView=new CompanyView("1",days[j],typeView.getTypeName());
            daysList.add(companyView);
        }

        HzAdapter hzAdapter=new HzAdapter(mContext,daysList);
        viewHolder.type_recycler_view.setAdapter(hzAdapter);
        hzAdapter.notifyDataSetChanged();



    }

    @Override
    public int getItemCount() {
        return mTypes.size();
    }

    @Override
    public Filter getFilter() {
        return mTypeFilter;
    }

    private Filter mTypeFilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TypeView> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mTypesFull);
            } else {
                String filterpattern = constraint.toString().toLowerCase().trim();

                for (TypeView cv : mTypesFull) {
                    if (cv.getTypeName().toLowerCase().contains(filterpattern)) {
                        filteredList.add(cv);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            mTypes.clear();
            mTypes.addAll((List) results.values);
            notifyDataSetChanged();

        }
    };


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView typeName;
        private RecyclerView type_recycler_view;

        public ViewHolder(View itemView) {
            super(itemView);

            typeName=itemView.findViewById(R.id.text_type);
            type_recycler_view=itemView.findViewById(R.id.hz_recycler_view);

        }

    }
}
