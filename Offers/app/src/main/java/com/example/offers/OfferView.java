package com.example.offers;

public class OfferView {

    private String view_logo,view_name,view_desc,c_name;

    public OfferView() {

    }

    public OfferView(String view_logo, String view_name, String view_desc,String c_name) {
        this.view_logo = view_logo;
        this.view_name = view_name;
        this.view_desc = view_desc;
        this.c_name=c_name;
    }

    public String getView_logo() {
        return view_logo;
    }

    public void setView_logo(String view_logo) {
        this.view_logo = view_logo;
    }

    public String getView_name() {
        return view_name;
    }

    public void setView_name(String view_name) {
        this.view_name = view_name;
    }

    public String getView_desc() {
        return view_desc;
    }

    public void setView_desc(String view_desc) {
        this.view_desc = view_desc;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }
}
