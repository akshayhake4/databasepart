package com.example.offers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter1 extends FragmentStatePagerAdapter {

    int mNoOfTabs;
    public PagerAdapter1(FragmentManager fm, int NumberOfTabs)
    {
        super(fm);
        this.mNoOfTabs=NumberOfTabs;
    }
    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                MyTeam myTeam=new MyTeam();
                return myTeam;
            case 1:
                Available_Teams available_teams=new Available_Teams();
                return available_teams;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }

}

