package com.example.offers;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Available_Teams#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Available_Teams extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ConnectionClass connectionClass;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Available_Teams() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Available_Teams.
     */
    // TODO: Rename and change types and number of parameters
    public static Available_Teams newInstance(String param1, String param2) {
        Available_Teams fragment = new Available_Teams();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    private ArrayList<FieldView> mobj=new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_available_teams,container,false);
       /* FieldView obj=new FieldView("Birds of a Feather",100);
        mobj.add(obj);
        obj=new FieldView("Dream Team",100);
        mobj.add(obj);
        obj=new FieldView("Great Mates",100);
        mobj.add(obj);
        obj=new FieldView("Brothers From Another Mother",100);
        mobj.add(obj);
        obj=new FieldView("Sistas from Different Mistas",100);
        mobj.add(obj);
        obj=new FieldView("Public Square",100);
        mobj.add(obj);
        obj=new FieldView("All in the Mind",100);
        mobj.add(obj);*/

       connectionClass=new ConnectionClass();

        Connection conn=connectionClass.CONN();

        String q="select teamname from Team GROUP by Team.teamname";

        try {
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(q);

            while(rs.next())
            {
                String tname=rs.getString("teamname");
                Statement statement=conn.createStatement();
                String query="select UserTeam.teamid,teamname,sum(earnedpoints) as total from Team,UserTeam,points where UserTeam.teamid=all(select teamid from Team where Team.teamname='"+tname+"') and UserTeam.userid=points.userid and Team.teamid=UserTeam.teamid GROUP by UserTeam.teamid";
                ResultSet res=statement.executeQuery(query);
                if(res.next()) {
                    FieldView obj = new FieldView(tname, res.getInt("total"), Teams.id);
                    mobj.add(obj);
                }
                else
                {
                    FieldView obj = new FieldView(tname, 0, Teams.id);
                    mobj.add(obj);
                }
            }
        } catch (SQLException e) {
            Toast.makeText(getActivity(),e.getMessage().toString()+"  "+Teams.id,Toast.LENGTH_LONG).show();
        }


        RecyclerView recyclerView=view.findViewById(R.id.recycler_view);
        GroupRecyclerViewAdapter adapter=new GroupRecyclerViewAdapter(getActivity(),mobj);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void initImageBitmaps()
    {

    }

    private  void initRecyclerView()
    {

    }
}
