package com.example.offers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddTeam extends AppCompatActivity {

    CircleImageView logo;
    private EditText tname,tdesc;
    ImageButton uploadphoto;
    Button btn_add;
    private static final int GALLERY_INTENT = 2;
    ConnectionClass connectionClass;
    int id=0;
    int id1=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team);

        logo=findViewById(R.id.team_logo);
        tname=findViewById(R.id.team_name);
        tdesc=findViewById(R.id.teamdesc);
        uploadphoto=findViewById(R.id.btnuploadphoto);
        btn_add=findViewById(R.id.btn_done);

        connectionClass=new ConnectionClass();



        uploadphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,GALLERY_INTENT);
            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name,desc;
                name=tname.getText().toString();
                desc=tdesc.getText().toString();

                if(TextUtils.isEmpty(name))
                {
                    Toast.makeText(AddTeam.this,"Enter Team name",Toast.LENGTH_SHORT).show();
                }
                else {

                    Connection con=connectionClass.CONN();

                    String q="select userid from User order by userid desc limit 1";
                    Statement stmt1 = null;
                    try {
                        stmt1 = con.createStatement();

                    ResultSet rs=stmt1.executeQuery(q);

                    if(rs.next())
                        id=rs.getInt("userid");

                    q="select teamid from Team order by teamid desc limit 1";
                    stmt1 = con.createStatement();
                    rs=stmt1.executeQuery(q);

                    if(rs.next())
                        id1=rs.getInt("teamid");

                    if(id1>id)
                        id=id1;
                    id++;

                        Calendar calendar=Calendar.getInstance();
                        String time=calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH)+" "+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);
                    String prof="1";
                    String ref="xyz";


                    String query="insert into Team values("+id+",'"+tname.getText().toString()+"','"+tdesc.getText().toString()+"','"+prof+"','"+time+"','"+time+"')";

                    Statement stmt = con.createStatement();
                    stmt.executeUpdate(query);



                        Toast.makeText(AddTeam.this,"Team "+name+" added successfully",Toast.LENGTH_SHORT).show();



                    } catch (SQLException e) {
                        Toast.makeText(AddTeam.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                    }




                    Intent intent=new Intent(AddTeam.this,Teams.class);
                    intent.putExtra("userid",getIntent().getIntExtra("userid",0));
                    startActivity(intent);
                    finish();
                }
            }
        });



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode== GALLERY_INTENT && resultCode== RESULT_OK)
        {




            Uri uri=data.getData();
            Picasso.with(AddTeam.this).load(uri.toString()).into(logo);









        }

    }
}
